#include <GL/gl.h>
#include <GL/glut.h>
#include <math.h>

struct RGB
{
    double R;
    double G;
    double B;
};

struct HSV
{
    double H;
    double S;
    double V;
};

struct RGB HSVtoRGB(struct HSV hsv) {
    double r = 0, g = 0, b = 0;

    if (hsv.S == 0) {
        r = hsv.V;
        g = hsv.V;
        b = hsv.V;
    }
    else {
        int i;
        double f, p, q, t;

        if (hsv.H == 360)
            hsv.H = 0;
        else
            hsv.H = hsv.H / 60;
        i = (int)trunc(hsv.H);
        f = hsv.H - i;

        p = hsv.V * (1.0 - hsv.S);
        q = hsv.V * (1.0 - (hsv.S * f));
        t = hsv.V * (1.0 - (hsv.S * (1.0 - f)));

    switch (i)
    {
    case 0:
        r = hsv.V;
        g = t;
        b = p;
        break;

    case 1:
        r = q;
        g = hsv.V;
        b = p;
        break;

    case 2:
        r = p;
        g = hsv.V;
        b = t;
        break;

    case 3:
        r = p;
        g = q;
        b = hsv.V;
        break;

    case 4:
        r = t;
        g = p;
        b = hsv.V;
        break;
    
    default:
        r = hsv.V;
        g = p;
        b = q;
        break;
    }
    }

    struct RGB rgb;
    rgb.R = r;
    rgb.G = g;
    rgb.B = b;

    return rgb;
}

void display() {
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_POINTS);
    struct HSV warna = {0, 0, 0};
    struct RGB hasil = HSVtoRGB(warna);
    glColor3f(hasil.R, hasil.G, hasil.B);
    glVertex2i(0, 0);
    glEnd();
    glFlush();
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutCreateWindow("titik opengl");
    glutInitWindowSize(300, 300);
    glutInitWindowPosition(0, 0);
    glutDisplayFunc(display);
    glutMainLoop();
    return 0;
}